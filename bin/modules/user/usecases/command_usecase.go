package usecases

import (
	"encoding/json"
	"fmt"
	"majoo-test-case/bin/config"
	"majoo-test-case/bin/modules/user/models/domain"
	"majoo-test-case/bin/modules/user/repositories/queries"
	httpError "majoo-test-case/bin/pkg/http-error"
	"majoo-test-case/bin/pkg/token"
	"majoo-test-case/bin/pkg/utils"
)

type userCommandUsecase struct {
	userQuery queries.User
}

// NewUserCommandUsecase initiation
func NewUserCommandUsecase(userQuery queries.User) *userCommandUsecase {
	return &userCommandUsecase{
		userQuery: userQuery,
	}
}

func (c *userCommandUsecase) Login(payload *domain.LoginPayload) utils.Result {
	var result utils.Result

	username := payload.Username
	password := payload.Password

	user, err := c.userQuery.FindOneByUsername(username)
	if err != nil {
		errObj := httpError.NewNotFound()
		errObj.Message = fmt.Sprintf("%v", err)
		result.Error = errObj
		return result
	}

	checkPassword := utils.CheckPasswordHash(password, user.Password)
	if !checkPassword {
		errObj := httpError.NewUnauthorized()
		errObj.Message = "password not match"
		result.Error = errObj
		return result
	}

	claim := token.Claim{
		ID: user.ID,
		Username: user.Username,
	}

	jwt := token.Generate(config.GlobalEnv.PrivateKey, &claim, config.GlobalEnv.AccessTokenExpired)
	if jwt.Error != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", jwt.Error)
		result.Error = errObj
		return result
	}

	jwtRefresh := token.GenerateRefreshToken(config.GlobalEnv.RefreshPrivateKey, &claim, config.GlobalEnv.RefreshTokenExpired)
	if jwtRefresh.Error != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", jwtRefresh.Error)
		result.Error = errObj
		return result
	}

	data := domain.LoginResponse{
		ID:	user.ID,
		Username: user.Username,
		AccessToken: jwt.Data.(string),
		RefreshToken: jwtRefresh.Data.(string),
	}

	result.Data = data
	return result
}

func (c *userCommandUsecase) RefreshToken(payload *domain.RefreshTokenRequest) utils.Result {
	var result utils.Result

	accessToken := payload.AccessToken

	validate := token.Validate(config.GlobalEnv.RefreshPublicKey, accessToken)
	if validate.Error != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", validate.Error)
		result.Error = errObj
		return result
	}

	var claim token.Claim
	data, _ := json.Marshal(validate.Data)

	err := json.Unmarshal(data, &claim)
	if err != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", err.Error())
		result.Error = errObj
		return result
	}

	jwt := token.Generate(config.GlobalEnv.PrivateKey, &claim, config.GlobalEnv.AccessTokenExpired)
	if jwt.Error != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", jwt.Error)
		result.Error = errObj
		return result
	}

	jwtRefresh := token.GenerateRefreshToken(config.GlobalEnv.RefreshPrivateKey, &claim, config.GlobalEnv.RefreshTokenExpired)
	if jwtRefresh.Error != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", jwtRefresh.Error)
		result.Error = errObj
		return result
	}

	result.Data = domain.LoginResponse{
		ID:	claim.ID,
		Username: claim.Username,
		AccessToken: jwt.Data.(string),
		RefreshToken: jwtRefresh.Data.(string),
	}

	return result
}
