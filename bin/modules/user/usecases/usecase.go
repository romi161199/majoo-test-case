package usecases

import (
	"majoo-test-case/bin/modules/user/models/domain"
	"majoo-test-case/bin/pkg/utils"
)

//CommandUsecase
type CommandUsecase interface {
	Login(payload *domain.LoginPayload)	utils.Result
	RefreshToken(payload *domain.RefreshTokenRequest) utils.Result
}