package domain

type User struct {
	ID			string	`json:"id" gorm:"column:id"`
	Name		string	`json:"name" gorm:"column:name"`
	Username	string 	`json:"user_name" gorm:"column:user_name"`
	Password	string 	`json:"password" gorm:"column:password"`
	CreatedAt	string 	`json:"created_at" gorm:"column:created_at"`
	CreatedBy	string 	`json:"created_by" gorm:"column:created_by"`
	UpdatedAt	string 	`json:"updated_at" gorm:"column:updated_at"`
	UpdatedBy	string 	`json:"updated_by" gorm:"column:updated_by"`
}

type InsertUser struct {
	Name		string	`json:"name" gorm:"column:name"`
	Username	string 	`json:"user_name" gorm:"column:user_name"`
	Password	string 	`json:"password" gorm:"column:password"`
}

type LoginPayload struct {
	Username	string `json:"username"`
	Password	string `json:"password"`
}

//RefreshTokenRequest struct
type RefreshTokenRequest struct {
	AccessToken string `json:"access_token"`
}


// LoginResponse struct
type LoginResponse struct {
	ID           string `json:"id"`
	Username     string `json:"username"`
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}