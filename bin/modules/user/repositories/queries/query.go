package queries

import "majoo-test-case/bin/modules/user/models/domain"

type User interface {
	FindOneByUsername(username string) (*domain.User, error)
}