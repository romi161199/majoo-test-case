package queries

import (
	"gorm.io/gorm"
	"majoo-test-case/bin/modules/user/models/domain"
)

//UserQuery Model
type UserQuery struct {
	db		*gorm.DB
}

func NewUserQuery(db *gorm.DB) *UserQuery {
	return &UserQuery{
		db: db,
	}
}

func (q *UserQuery) FindOneByUsername(username string) (*domain.User, error) {
	var result domain.User

	err := q.db.Table("Users").Where("user_name = ?", username).Find(&result).Error

	if err != nil {
		return nil, err
	}

	return &result, nil
}

