package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"majoo-test-case/bin/modules/user/models/domain"
	"majoo-test-case/bin/modules/user/repositories/queries"
	"majoo-test-case/bin/modules/user/usecases"
	"majoo-test-case/bin/pkg/databases"
	httpError "majoo-test-case/bin/pkg/http-error"
	"majoo-test-case/bin/pkg/utils"
	"net/http"
)

type UserHTTPHandler struct {
	userCommandUsecase	usecases.CommandUsecase
}

//New initiation
func New() *UserHTTPHandler {
	db := databases.MySQLInit()
	userQuery := queries.NewUserQuery(db)
	userCommandUsecase := usecases.NewUserCommandUsecase(userQuery)
	return &UserHTTPHandler{
		userCommandUsecase: userCommandUsecase,
	}
}


// Mount function
func (u *UserHTTPHandler) Mount(user *echo.Group) {
	user.POST("/login", u.Login)
	user.POST("/refresh-token", u.RefreshToken)
}

func (u *UserHTTPHandler) Login(c echo.Context) error {
	body, _ := ioutil.ReadAll(c.Request().Body)

	var user domain.LoginPayload
	err := json.Unmarshal(body, &user)
	if err != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", err.Error())
		return utils.ResponseError(errObj, c)
	}
	result := u.userCommandUsecase.Login(&user)
	if result.Error != nil {
		return utils.ResponseError(result.Error, c)
	}

	return utils.Response(result.Data, "Success Login User", http.StatusOK, c)
}

func (u *UserHTTPHandler) RefreshToken(c echo.Context) error {
	body, _ := ioutil.ReadAll(c.Request().Body)

	var user domain.RefreshTokenRequest
	err := json.Unmarshal(body, &user)
	if err != nil {
		errObj := httpError.NewBadRequest()
		errObj.Message = fmt.Sprintf("%v", err.Error())
		return utils.ResponseError(errObj, c)
	}

	result := u.userCommandUsecase.RefreshToken(&user)
	if result.Error != nil {
		return utils.ResponseError(result.Error, c)
	}

	return utils.Response(result.Data, "Success Refresh Token User", http.StatusOK, c)
}