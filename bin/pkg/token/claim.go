package token


type Claim struct {
	ID			string `json:"id"`
	Username	string `json:"username"`
}
