package token

import (
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"majoo-test-case/bin/pkg/utils"
	"time"
)

func Generate(key *rsa.PrivateKey, payload *Claim, expired time.Duration) utils.Result {
	var output utils.Result

		now := time.Now()
		exp := now.Add(expired)

		token := jwt.New(jwt.SigningMethodRS256)

		claims := jwt.MapClaims{
			"iss": "majoo",
			"exp": exp.Unix(),
			"iat": now.Unix(),
			"sub": payload.ID,
			"username": payload.Username,
			"aud": "97b33193-43ff-4e58-9124-b3a9b9f72c34",
		}

		token.Claims = claims

		tokenString, err := token.SignedString(key)
		if err != nil {
			output = utils.Result{Error: err}
			return output
		}

		output = utils.Result{Data: tokenString}

	return output
}

func GenerateRefreshToken(key *rsa.PrivateKey, payload *Claim, expired time.Duration) utils.Result {
	var output utils.Result

	now := time.Now()
	exp := now.Add(expired)

	token := jwt.New(jwt.SigningMethodRS256)

	claims := jwt.MapClaims{
		"iss": "majoo",
		"exp": exp.Unix(),
		"iat": now.Unix(),
		"sub": payload.ID,
		"username": payload.Username,
		"aud": "97b33193-43ff-4e58-9124-b3a9b9f72c34",
	}

	token.Claims = claims

	tokenString, err := token.SignedString(key)
	if err != nil {
		output = utils.Result{Error: err}
		return output
	}

	output = utils.Result{Data: tokenString}

	return output
}

func Validate(publicKey *rsa.PublicKey, tokenString string) utils.Result {
	var output utils.Result

	tokenParse, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})

	var errToken string
	switch ve := err.(type) {
	case *jwt.ValidationError:
		if ve.Errors == jwt.ValidationErrorExpired {
			errToken = "token has been expired"
		} else {
			errToken = "token parsing error"
		}
	}

	if len(errToken) > 0 {
		output = utils.Result{Error: errToken}
		return output
	}

	if !tokenParse.Valid {
		output = utils.Result{Error: "token parsing error"}
		return output
	}

	mapClaims, _ := tokenParse.Claims.(jwt.MapClaims)
	var tokenClaim Claim
	tokenClaim.ID, _ = mapClaims["sub"].(string)
	tokenClaim.Username, _ = mapClaims["username"].(string)

	output = utils.Result{Data: tokenClaim}

	return output
}