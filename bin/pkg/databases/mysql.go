package databases

import (
	"fmt"
	"majoo-test-case/bin/config"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func MySQLInit() *gorm.DB {

	dbHost := config.GlobalEnv.MySQLHost
	dbPort := config.GlobalEnv.MySQLPort
	dbUser := config.GlobalEnv.MySQLUser
	dbPwd := config.GlobalEnv.MYSQLPassword
	dbScheme := config.GlobalEnv.MYSQLDBScheme

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbUser, dbPwd, dbHost, dbPort, dbScheme)

	// Open Connection
	db, err := gorm.Open(mysql.Open(dsn))

	if err != nil {
		panic("Can't connect to database : " + err.Error())
	}

	return db
}
