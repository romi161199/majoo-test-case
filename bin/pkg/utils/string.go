package utils

import (
	"crypto/md5"
	"fmt"
)

func HashPassword(password string) string {
	data := md5.Sum([]byte(password))

	hashPassword := fmt.Sprintf("%x", data)

	return hashPassword
}

func CheckPasswordHash(password, hash string) bool {
	hashPassword := HashPassword(password)

	return hashPassword == hash
}
