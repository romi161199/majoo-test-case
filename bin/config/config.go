package config

import (
	"crypto/rsa"
	"github.com/joho/godotenv"
	"log"
	"majoo-test-case/bin/config/key"
	"os"
	"strings"
	"time"
)

type Env struct {
	RootApp			string
	HTTPPort		string
	MySQLHost		string
	MySQLPort		string
	MySQLUser		string
	MYSQLPassword 	string
	MYSQLDBScheme	string
	PrivateKey         		*rsa.PrivateKey
	RefreshPrivateKey       *rsa.PrivateKey
	PublicKey          		*rsa.PublicKey
	RefreshPublicKey        *rsa.PublicKey
	AccessTokenExpired 		time.Duration
	RefreshTokenExpired 	time.Duration
}

// GlobalEnv global environment
var GlobalEnv Env

func init() {
	err := godotenv.Load()

	if err != nil {
		log.Println(err)
	}

	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}

	rootApp := strings.TrimSuffix(path, "/bin/config")
	os.Setenv("APP_PATH", rootApp)
	GlobalEnv.RootApp = rootApp
	GlobalEnv.PrivateKey = key.LoadPrivateKey()
	GlobalEnv.RefreshPrivateKey = key.LoadRefreshPrivateKey()
	GlobalEnv.PublicKey = key.LoadPublicKey()
	GlobalEnv.RefreshPublicKey = key.LoadRefreshPublicKey()

	var ok bool

	GlobalEnv.HTTPPort, ok = os.LookupEnv("HTTP_PORT")
	if !ok {
		panic("missing HTTP_PORT environment")
	}

	GlobalEnv.MySQLHost, ok = os.LookupEnv("MYSQL_DB_HOST")
	if !ok {
		panic("missing MYSQL_DB_HOST environment")
	}

	GlobalEnv.MySQLPort, ok = os.LookupEnv("MYSQL_DB_PORT")
	if !ok {
		panic("missing MYSQL_DB_PORT environment")
	}

	GlobalEnv.MySQLUser, ok = os.LookupEnv("MYSQL_DB_USER")
	if !ok {
		panic("missing MYSQL_DB_USER environment")
	}

	GlobalEnv.MYSQLPassword, ok = os.LookupEnv("MYSQL_DB_PASSWORD")
	if !ok {
		panic("missing MYSQL_DB_PASSWORD environment")
	}

	GlobalEnv.MYSQLDBScheme, ok = os.LookupEnv("MYSQL_DB_SCHEME")
	if !ok {
		panic("missing MYSQL_DB_SCHEME environment")
	}

	GlobalEnv.AccessTokenExpired, err = time.ParseDuration("60m")
	if err != nil {
		panic("failed parsing AccessTokenExpired")
	}

	GlobalEnv.RefreshTokenExpired, err = time.ParseDuration("120m")
	if err != nil {
		panic("failed parsing AccessTokenExpired")
	}
}