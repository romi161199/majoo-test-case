package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"majoo-test-case/bin/config"
	userHTTPHandler "majoo-test-case/bin/modules/user/handlers"
)

func main() {
	//Echo instance
	e := echo.New()

	e.Use(middleware.Recover())

	userGroup := e.Group("/users")
	userHTTP := userHTTPHandler.New()
	userHTTP.Mount(userGroup)

	listenerPort := fmt.Sprintf(":%s", config.GlobalEnv.HTTPPort)
	e.Logger.Fatal(e.Start(listenerPort))
}
